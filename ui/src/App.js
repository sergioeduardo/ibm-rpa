import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';

const eventService = 'https://peaceful-lake-60590.herokuapp.com/ticketMaster/getEvent?event=';

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: 200,
    },
  },
}));


function App() {

  const [inputText, setInputText] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [handleErrors, setHandleErrors] = useState([]);
  const [isLoadData, setIsLoadData] = useState(false);
  const [ dataFetch, setDataFetch] = useState(false);


  function changeInputText(e) {
    const value = e.target.value;
    setInputText(value);
  }


  async function loadData(event) {

    try {

      const url = eventService + event;

      setDataFetch(true);
      const response = await fetch(url);
      const results = await response.json();

      console.log(results._embedded.events);

      setSearchResults(results._embedded.events);
      setIsLoadData(true);
      setDataFetch(false);


    } catch (error) {
      setHandleErrors(handleErrors.concat(error));
      setDataFetch(false);
    }


  }

  function newSearch(e) {

    e.preventDefault();
    loadData(inputText);

  }

  async function sendToLogs() {
    const url = 'https://coe-logging.herokuapp.com/logTransaction';

    const data = new FormData();

    data.append("message", JSON.stringify(searchResults));
    data.append("status", 200);
    data.append("service", "ibm robotic process automation");

    try {

      setDataFetch(true);

      const response = await fetch(url, {
        method: 'POST',
        body: data
      });

      setIsLoadData(false);
      setDataFetch(false);
      setSearchResults([]);

    } catch (error) {

      setHandleErrors(handleErrors.concat(error));
      setDataFetch(false);

    }

  }


  const classes = useStyles();

  return (
    <div className='app' >

      <div>
        <Typography
          component="span"
          variant="body2"
          className={classes.inline}
          color="textPrimary"
        >
          Ibm Robotic Process Automation
                        </Typography>

      </div>

      <form className={classes.root} noValidate autoComplete="off">
        <TextField

          id="standard-basic"
          label="Event"
          value={inputText}
          onChange={changeInputText}

        />

        <Button
          style={{ marginTop: '21px' }}
          variant="contained"
          color="primary"
          onClick={newSearch}
        >
          Search events
        </Button>

      </form>
      {
        dataFetch ?  <LinearProgress /> : null
      }
     

      <List style={{
        width: '100%',
        paddingLeft: '15%',
        paddingRight: '15%'
      }}>
        {
          searchResults.map((item) => {
            return (
              <div>
                <ListItem alignItems="flex-start">

                  <ListItemText
                    primary={item.name}
                    secondary={
                      <React.Fragment>
                        <Typography
                          component="span"
                          variant="body2"
                          className={classes.inline}
                          color="textPrimary"
                        >
                          Start:
                        </Typography>
                        {' ' + item.dates.start.localDate}
                      </React.Fragment>
                    }
                  />
                </ListItem>
                <Divider variant="inset" component="li" />
              </div>

            )
          })
        }
      </List>
      {
        isLoadData ? (
          <Button
            style={{ marginTop: '21px' }}
            variant="contained"
            color="primary"
            onClick={sendToLogs}
          >
            Send to logs
        </Button>
        ) : null
      }


    </div>
  );
}

export default App;
