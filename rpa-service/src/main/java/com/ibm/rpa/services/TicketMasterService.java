package com.ibm.rpa.services;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TicketMasterService {

    public TicketMasterService(){}

    public String getEventArtist(String event) {
        try {
            final String urlTicketMaster = "http://app.ticketmaster.com/discovery/v1/events.json?keyword="
                    + event + "&apikey=c69Z588ivP77fxHOMz2KDZMyKgQZ8fk2&callback=myFunction";

            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.getForObject(urlTicketMaster, String.class);
        }
        catch (Exception ex){
            throw ex;
        }
    }
}
