package com.ibm.rpa.controllers;

import com.ibm.rpa.services.TicketMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ticketMaster")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET })
public class TicketMasterController {
    private final TicketMasterService ticketMasterService;

    @Autowired
    public TicketMasterController(TicketMasterService ticketMasterService) {
        this.ticketMasterService = ticketMasterService;
    }

    @ResponseBody
    @GetMapping(value = "/getEvent")
    public String getEvent(@RequestParam(name = "event") String event) {
        try {
            return ticketMasterService.getEventArtist(event);
        }
        catch (Exception ex) {
            throw ex;
        }
    }
}
